# First Name,Last Name,Email Address,Program Stream,Opt-In
# Generating 5 instances, no matching rules except no repeats
# Groups of 3
# 71 people, last group is a group of 2

from pandas import read_excel
from tkinter import Tk
from tkinter.filedialog import askopenfilename
import math

from grad import Grad

def generateHeader(nInGroup):
	header = 'Group'

	for i in range(nInGroup):
		header += ',First Name #{0},Last Name #{0},Email #{0},Stream #{0}'.format(i+1)

	header += '\n'

	return header

def parseSpreadsheet(nInGroup, filename):
	grads = []

	data = read_excel(filename)

	# Iterate through rows of table and create grad objects
	for row in data.index:

		if data.at[row, 'Opt-In'] == 'N':
			continue

		firstName = data.at[row, 'First Name']
		lastName = data.at[row, 'Last Name']
		email = data.at[row, 'Email Address']
		stream = data.at[row, 'Program Stream']

		grads.append(Grad(firstName, lastName, email, stream))

	# Add dummy objects until we get a whole number of groups
	while len(grads) % nInGroup != 0:
		grads.append(Grad('', '', '', 'Dummy'))

	return grads

def static(name, value):
	def decorate(func):
		setattr(func, name, value)
		return func	

	return decorate

@static("primeList", [2])
@static("n", 0)
def generatePrimeNumber():

	n = generatePrimeNumber.n
	primeList = generatePrimeNumber.primeList

	prime = 0

	# Use one as "prime number" first
	if n == 0:
		prime = 1
	elif n == 1:
		prime = 2
	else:
		# first number to test if prime
		lastPrime = primeList[-1]
		num = 3 if lastPrime == 2 else lastPrime

		# keep generating primes until we get to the nth one
		while len(primeList) < n:

			# check if num is divisible by any prime before i
			for p in primeList:
				# if there is no remainder dividing the number
				# then the number is not a prime
				if num % p == 0:
					# break to stop testing more numbers, we know it's not a prime
					break

			# If it is a prime, then add it to the list
			else:
				# Store prime number
				prime = num

				# Append number to prime list
				primeList.append(num)

			# Ignore even numbers as they aren't prime
			num += 2

	generatePrimeNumber.n += 1
	generatePrimeNumber.primeList = primeList

	# return the last prime number generated
	return prime

def generateSolution(nInstances, nInGroup, grads):
	# Generate a dictionary, where each instance/index is a key,
	# and the value is a list of n-tuples representing the groups
	solution = {}

	for i in range(nInstances):
		solution[i] = generateInstance(nInGroup, grads)

	return solution

def generateInstance(nInGroup, grads):

	nGrads = len(grads)
	nGroups = math.ceil(nGrads/nInGroup)

	instance = []

	# Keep iterating until we have a valid instance
	while True:
		
		# Use prime numbers to partition the list into groups
		prime = generatePrimeNumber()

		instance = []
		shuffledGrads = []

		start = 0

		# Use the prime number until all the grads have been exhausted
		while len(shuffledGrads) < nGrads:
			shuffledGrads += grads[start::prime]
			start += 1

		for i in range(nGroups):
			start = i * nInGroup
			end = (i + 1) * nInGroup
			instance.append(tuple(shuffledGrads[start:end]))

		if isValidInstance(instance):
			break

	# If the instance is valid, store all the new partners
	updatePartners(instance)

	# Return the valid instance
	return instance

def isValidInstance(instance):
	# An instance is a list of n-tuples
	# Need to validate that all the people in the n-tuple have never
	# been partnered with each other before

	# Iterate through each tuple
	for group in instance:
		# If any group member has partnered with another,
		# this instance is invalid
		for i in range(len(group)):
			main = group[i]
			others = group[:i] + group[i+1:]

			for other in others:
				if main.isPartnered(other):
					return False

	# If we make it to the end, all is good
	return True

def updatePartners(instance):

	for group in instance:
		for i in range(len(group)):
			main = group[i]
			others = group[:i] + group[i+1:]

			for other in others:
				main.addPartner(other)

def writeOutput(nInGroup, solution):

	header = generateHeader(nInGroup)

	# Write each instance
	for i in solution:

		instance = solution[i]

		# Create a new csv file for each instance
		with open('wheelofgrads{}.csv'.format(i), 'w') as file:

			# Write csv header
			file.write(header)

			for i in range(len(instance)):
				# Write group number
				output = "{}".format(i)

				for grad in instance[i]:
					if grad.stream == "Dummy":
						continue

					output += ",{},{},{},{}".format(grad.firstName, grad.lastName, grad.email, grad.stream)

				# Add new line to output
				output += "\n"

				file.write(output)

if __name__ == '__main__':
	Tk().withdraw() 

	# Default values for problem specification
	nInstances = 5
	nInGroup = 3

	# TODO: Get size of groups, number of instances from user

	filename = askopenfilename()

	grads = parseSpreadsheet(nInGroup, filename)

	solution = generateSolution(nInstances, nInGroup, grads)

	writeOutput(nInGroup, solution)