# Wheel of Grads generator

## Before you run
Make sure you have a .csv file with all the graduate data. The program assumes you have a spreadsheet that contains the following columns (exact spelling required):

- First Name
- Last Name
- Email Address
- Program Stream

You can save as/export a .csv file from Excel or Google Sheets.

## Running with executable
Run `wog.exe`, select the .csv file that contains your data, and wait for the program to run.

## Running from terminal
If you have problems running the executable (like an overzealous virus scanner), you can run as a normal Python program:
`python wog.py`

The program will run and from this point both options behave the same.