
class Grad:

	def __init__(self, firstName, lastName, email, stream):
		self.firstName = firstName
		self.lastName = lastName
		self.email = email
		self.stream = stream

		self.partners = []

	def isPartnered(self, potentialPartner):
		return potentialPartner in self.partners

	def addPartner(self, potentialPartner):
		self.partners.append(potentialPartner)

	def __repr__(self):
		return "{} {}".format(self.firstName, self.lastName)

	def __str__(self):
		return "{} {} - Email: {}, Stream: {}".format(
			self.firstName, self.lastName, self.email, self.stream)

	def __eq__(self, other):
		return self.firstName == other.firstName and self.lastName == other.lastName and self.email == other.email and self.stream == other.stream