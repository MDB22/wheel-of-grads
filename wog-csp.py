# First Name,Last Name,Email Address,Program Stream,Opt-In
# Generating 5 instances, no matching rules except no repeats
# Groups of 3
# 71 people, last group is a group of 2

from pandas import read_excel
from tkinter import Tk
from tkinter.filedialog import askopenfilename
from constraint import *

from grad import Grad

N_INSTANCES = 2
N_IN_GROUP = 3

def generateHeader(nInGroup):
	header = 'Group'

	for i in range(nInGroup):
		header += ',First Name #{0},Last Name #{0},Email #{0},Stream #{0}'.format(i+1)

	header += '\n'

	return header

def parseSpreadsheet(filename):
	grads = {}
	index = 0

	data = read_excel(filename)

	# Iterate through rows of table and create grad objects
	for row in data.index:

		if data.at[row, 'Opt-In'] == 'N':
			continue

		firstName = data.at[row, 'First Name']
		lastName = data.at[row, 'Last Name']
		email = data.at[row, 'Email Address']
		stream = data.at[row, 'Program Stream']

		grads[index] = Grad(firstName, lastName, email, stream)
		index += 1

	return grads

def writeOutput(nGrads, nInstances, nInGroup, grads, solution):

	header = generateHeader(nInGroup)

	# Write each instance
	for i in range(nInstances):

		# Create a new csv file for each instance
		with open('wheelofgrads{}.csv'.format(i), 'w') as file:

			# Write csv header
			file.write(header)

			for gradNumber in range(nGrads):
				# Write group number
				output = "{}".format(gradNumber)

				# Write group "owner"
				index = gradNumber
				grad = grads[index]
				output += ",{},{},{},{}".format(grad.firstName, grad.lastName, grad.email, grad.stream)

				# Write member 1 of instance i
				index = gradNumber * nInstances * (nInGroup - 1) + i * (nInGroup - 1)
				grad = grads[solution[index]]
				output += ",{},{},{},{}".format(grad.firstName, grad.lastName, grad.email, grad.stream)

				# Write member 2 of instance i
				index += 1
				grad = grads[solution[index]]
				output += ",{},{},{},{}".format(grad.firstName, grad.lastName, grad.email, grad.stream)

				# Add new line to output
				output += "\n"

				file.write(output)

if __name__ == '__main__':
	Tk().withdraw() 

	# TODO: Get size of groups, number of instances,
	# and spreadsheet, from user

	# filename = askopenfilename()
	filename = "C:/Repositories/wheelofgrads/grads.xlsx"

	grads = parseSpreadsheet(filename)

	problem = Problem(RecursiveBacktrackingSolver())

	nGrads = len(grads)
	nPartners = N_INSTANCES * (N_IN_GROUP - 1)
	nElements = nGrads * nPartners

	# Problem can be considered an (nGrads) x (nInstances * nInGroup-1) 
	# element two-dimensional array
	problem.addVariables(range(nElements), range(nGrads))

	# Add constraint that each "row" must contain different elements;
	# this should result in the grad being allocated different partners
	for row in range(nGrads):
		start = row * N_INSTANCES * (N_IN_GROUP - 1)
		end = (row + 1) * N_INSTANCES * (N_IN_GROUP - 1)
		problem.addConstraint(AllDifferentConstraint(), range(start, end))

	# Add constraint that each "column" must contain different elements;
	# this should ensure that all grads get allocated in each instance
	for column in range(nPartners):
		problem.addConstraint(AllDifferentConstraint(), range(column, nElements, nPartners))

	# Add constraint that a "row" can't contain its index;
	# this should ensure a grad doesn't partner themself
	for row in range(nGrads):
		start = row * N_INSTANCES * (N_IN_GROUP - 1)
		end = (row + 1) * N_INSTANCES * (N_IN_GROUP - 1)
		problem.addConstraint(NotInSetConstraint([row]), range(start, end))

	# Add constraint that an instance can't contain the same number twice;
	# this should ensure a person isn't in multiple groups in the same instance
	for instance in range(N_INSTANCES):

		indexes = []

		for row in range(nGrads):

			start = instance * (N_IN_GROUP - 1) + row * N_INSTANCES * (N_IN_GROUP - 1)
			end = (instance + 1) * (N_IN_GROUP - 1) + row * N_INSTANCES * (N_IN_GROUP - 1)
			
			indexes += list(range(start, end))

		problem.addConstraint(AllDifferentConstraint(), indexes)

	print("Problem set up, getting solution.")

	solution = problem.getSolution()

	print(solution)

	writeOutput(nGrads, N_INSTANCES, N_IN_GROUP, grads, solution)